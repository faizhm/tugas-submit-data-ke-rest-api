<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use Illuminate\Support\Str;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/key', function () {
    return $var = Str::random(32);
});

$router->get('books', 'BooksController@index');

$router->get('books/search/{id}', 'BooksController@cariId');

$router->post('books/store', 'BooksController@store');

$router->put('books/update/{id}', 'BooksController@update');

$router->delete('books/destroy/{id}', 'BooksController@destroy');
