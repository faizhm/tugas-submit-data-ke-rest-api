<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['_self', '_store', '_update', '_destroy', 'id_books'];
}
